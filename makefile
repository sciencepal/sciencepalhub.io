README.md:
	touch README.md
	echo "**The Unix Workbench**" > README.md
	echo "\n" >> README.md
	echo "**Title** of the project : Guessing Game" >> README.md
	echo "\n" >> README.md
	echo "**Date and Time** of make run: " >> README.md
	date >> README.md
	echo "\n" >> README.md
	echo "Number of **lines of code** in guessinggame.sh: " >> README.md
	cat guessinggame.sh | wc -l >> README.md
	echo "\n" >> README.md
	echo "**GitHub Pages Link**: https://sciencepal.github.io/" >> README.md
	echo "\n" >> README.md
