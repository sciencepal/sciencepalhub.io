#!/usr/bin/env bash

answer=0
correct=0
RED='\033[0;31m'
GREEN='\033[0;32m'
WHITE='\033[1;37m'

function get_guess {

	echo -e "${WHITE}Enter number of files in current directory"
	read files

	while [[ correct -eq 0 ]] ;
	do
		echo -e "${WHITE}Guess number of files in current directory"
		read answer
		check_answer
	done

}

function check_answer {

	if [[ $answer -gt $files ]] ;
	then
		echo -e "Your answer is ${RED}greater${WHITE}, ${RED}guess again${WHITE}!!"
	elif [[ $answer -lt $files ]] ;
	then
		echo -e "Your answer is ${RED}less${WHITE}, ${RED}guess again${WHITE}!!"
	else
		echo -e "${GREEN}Congrats${WHITE}, your guess was ${GREEN}correct${WHITE}!!"
		correct=1
	fi

}

get_guess
